import * as fs from 'fs';
import {EndpointCommunicator, IEndpointCommunicatorConfig} from "@devctrl/lib-communicator";
import * as SerialPort from "serialport";

import {
    Control,
    ControlData,
    ControlUpdateData,
    IEndpointStatus,
    IndexedDataSet
} from "@devctrl/common";

let debug = console.log;

export class DWPodiumButtonCommunicator extends EndpointCommunicator {

    port : SerialPort;
    ctid: string;
    value = [false, false, false, false];
    individualControls : Control[];
    lastConnectAttemptTime = 0;
    lastDetectAttemptTime = 0;

    constructor(config: IEndpointCommunicatorConfig) {
        super(config);
    }

    closeConnection() {
        if (this.port) {
            this.port.close(() => {
                this.updateStatus({connected: false});
                this.log(`serial port ${this.config.endpoint.address} closed`, EndpointCommunicator.LOG_CONNECTION);
            });
        }
        else {
            this.updateStatus({connected: false});
        }
    }

    connect() {
        // Rate limit connection attempts to once per 2 seconds
        if (Date.now() - this.lastConnectAttemptTime < 2000) {
            setTimeout(this.connect, 2000);
            return;
        }

        this.lastConnectAttemptTime = Date.now();

        this.port = new SerialPort(this.config.endpoint.address, { baudRate : 9600 });

        this.port.on('error', (err) => {
            this.log("serial port error: " + err.message, EndpointCommunicator.LOG_CONNECTION);
            this.updateStatus({connected: false});
        });

        this.port.on('open', () => {
            this.log(`serial port ${this.config.endpoint.address} opened`, EndpointCommunicator.LOG_CONNECTION);
            this.updateStatus({connected: true});
        });
    }

    /*
    Check for the existence of the serial port
     */
    detectPort() {
        // Rate limit connection attempts to once per seconds
        if (Date.now() - this.lastDetectAttemptTime < 1000) {
            setTimeout(this.detectPort, 1000);
            return;
        }

        this.lastDetectAttemptTime = Date.now();

        if (fs.existsSync(this.endpoint.address)) {
            this.updateStatus({reachable: true});
        }
        else {
            this.updateStatus({reachable: false});
        }
    }

    getControlTemplates() : IndexedDataSet<Control> {
        this.ctid = this.endpoint_id + "-state";
        let templateData : ControlData  = {
            _id: this.ctid,
            ctid: this.ctid,
            endpoint_id : this.endpoint_id,
            usertype: "button-set",
            name: "State",
            control_type: "object",
            poll: 0,
            ephemeral: false,
            config: {},
            value: this.value
        };

        let control = new Control(this.ctid, templateData);
        let ret = {};
        ret[this.ctid] = control;
        return <IndexedDataSet<Control>>ret;
    }


    handleControlUpdateRequest(request: ControlUpdateData) {

        if (this.port.isOpen) {
            let outChar = this.valueToChar(request.value);
            this.port.write(outChar);
            debug(`char ${outChar} sent to button controller`);

            let control = this.controls[request.control_id];
            this.config.controlUpdateCallback(control, request.value);
            control.value = request.value;
        }
        else {
            this.port.open();
        }
    }


    updateStatus(statusChanges: IEndpointStatus) {
        let statusDiff= this.config.endpoint.statusDiff(statusChanges);
        let statusUnchanged = this.config.endpoint.compareStatus(statusChanges);
        let es = this.epStatus;

        if (! (es === this.config.endpoint.epStatus)) {
            this.log("epStatus mismathc!!!", EndpointCommunicator.LOG_STATUS);
        }

        let diffStr = "";
        // Set the new values
        for (let f in statusDiff) {
            es[f] = statusDiff[f];
            diffStr += f;
            diffStr += " ";
        }

        // unused  higher states should all mirror connected
        es.loggedIn = es.polling = es.responsive = es.connected;
        es.ok = ( es.enabled && es.reachable && es.connected && es.loggedIn && es.polling && es.responsive);

        let statusStr = this.endpoint.statusStr;
        this.log("status update: " + statusStr, EndpointCommunicator.LOG_STATUS);

        if (! statusUnchanged) {
            //this.log("status diff: " + diffStr, EndpointCommunicator.LOG_STATUS);
            this.config.statusUpdateCallback();
        }


        // Figure out what to do next
        if (! es.enabled) {
            if (! es.reachable) {
                if (! es.connected) {
                    if (!es.ok) {
                        // Stopped.  Nothing to do.
                        return;
                    }
                    else {  // ok
                        if (es.messengerConnected) {
                            // Not really ok, update accordingly
                            this.updateStatus({ok: false});
                            return;
                        }
                    }
                }
                else { // connected
                    this.closeConnection();
                    return;
                }
            }
            else { // reachable
                // Reachable, not enabled, nothing to do.
                if (es.connected) {
                    this.closeConnection();
                    return;
                }
                else {
                    if (es.ok) {
                        // Not really ok, update accordingly
                        this.updateStatus({ok: false});
                        return;
                    }
                }
            }
        }
        else { // enabled
            if (! es.reachable) {
                // Enabled, not reachable, nothing to do
                this.detectPort();
                return;
            }
            else { // reachable
                if (! es.connected) {
                    this.connect();
                    return;
                }
                else { // connected
                    if (! es.ok) {
                        this.updateStatus({ok : true });
                        return;
                    }
                    else { // ok
                        return;
                    }
                }
            }
        }

        // We'll only fall through to here in weird unhandled cases.
        throw new Error("unhandled status update state");
    }

    valueToChar(value) {
        // Update value is a list of boolean values.  Convert
        // them into a hex value with bits corresponding to the
        // booleans.  Add 65 to this value to put the resulting
        // hex value in the ASCII range
        let outVal = 65;
        for (let i = 0; i < this.value.length; i++) {
            if (value[i]) {
                outVal += (1 << i);
            }
        }

        let outChar = String.fromCharCode(outVal);

        return outChar;
    }

}
