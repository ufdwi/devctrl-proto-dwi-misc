import {
    Control
} from "@devctrl/common";
import {
    HTTPCommunicator,
    IHTTPCommandConfig,
    HTTPCommand,
    IEndpointCommunicatorConfig } from "@devctrl/lib-communicator";
import {Pearl2TimestampsCommand} from "./Pearl2TimestampsCommand";

export class Pearl2Communicator extends HTTPCommunicator {
    endpointUser = "admin";
    endpointPassword = "DW2000";


    constructor(config: IEndpointCommunicatorConfig) {
        super(config);

        this.registerChannelRecordCommand("1");
        this.registerChannelRecordCommand("2");
        this.registerChannelRecordCommand("3");
        this.registerChannelRecordCommand("4");
        this.commands["timestamps"] = new Pearl2TimestampsCommand(this.endpoint_id);
    }

    registerChannelRecordCommand(chanNum) {
        let ctid = this.endpoint_id + `channel-${chanNum}-record`;
        this.commands[ctid] = new HTTPCommand(
            {
                name: `Channel ${chanNum} record`,
                cmdPathFunction: (val) => {
                    //let enabled = val ? "on" : "off";
                    //return `/admin/channel${chanNum}/set_params.cgi?rec_enabled=${enabled}`;
                    let enabled = val ? "start" : "stop";
                    return `/api/recorders/${chanNum}/control/${enabled}`;
                },
                cmdResponseRE: "",
                //cmdQueryPath: `/admin/channel${chanNum}/get_params.cgi?rec_enabled`,
                cmdQueryPath: `/api/recorders/${chanNum}/status`,
                cmdQueryResponseParseFn: (data) => {
                    let obj = JSON.parse(data);
                    return obj.result.state === "started";
                },
                updateMethod: "POST",
                controlData: {
                    _id: ctid,
                    ctid: ctid,
                    endpoint_id: this.endpoint_id,
                    usertype: Control.USERTYPE_SWITCH,
                    name: `Channel ${chanNum} record`,
                    control_type: Control.CONTROL_TYPE_BOOLEAN,
                    poll: 1,
                    ephemeral: false,
                    config: {},
                    value: false
                }
            }
        )
    }
}