import {
    IDCCommand, IDCHTTPCommand
} from "@devctrl/lib-communicator";
import {
    Control
} from "@devctrl/common";

export class Pearl2TimestampsCommand implements IDCHTTPCommand {
    endpointId: string;
    controls: Control[];
    name = "recorder status";
    writeonly = false;
    updateMethod = "GET";
    queryMethod = "GET";


    constructor(endpointId: string) {
        this.endpointId = endpointId;
        this.controls = [
            this.getControl("1"),
            this.getControl("2"),
            this.getControl("3"),
            this.getControl("4")
        ];
    }

    get ctidList() : string[] {
        return this.controls.map(c => c.ctid);
    }

    getControlTemplates() { return this.controls; }
    matchesReport(line: string) { return false; }
    matchQueryResponse(line: string) { return true; }
    matchUpdateResponse(control, update, line) { return false}

    parseQueryResponse(control: Control, line: string) {
        let respObj;
        try {
            respObj = JSON.parse(line);
        }
        catch(e) {
            throw new Error("error parsing JSON response: " + line);
        }

        if (Array.isArray(respObj.result)) {
            let statusObj = respObj.result.find((el) => {
                // Find the channel status object for this control
                return control.ctid.indexOf(`channel-${el.id}`) > -1;
            });

            if (statusObj && statusObj.status && statusObj.status.duration) {
                return statusObj.status.duration;
            }
        }

        return 0;
    }

    parseReportValue(control, line) {
        // No reports with this one
        return 0;
    }

    parseUpdateResponse(control,update, line) {
        // Readonly, no updates
        return 0;
    }

    queryString() {
        return "/api/recorders/status";
    }

    updateString(control, update) { return ''};

    private getControl(controlNum) {
        let ctid = `${this.endpointId}-channel-${controlNum}-duration`;
        return new Control(ctid,
            {
                _id: ctid,
                name: `Channel ${controlNum} Duration`,
                endpoint_id: this.endpointId,
                ctid: ctid,
                usertype: Control.USERTYPE_READONLY,
                control_type: Control.CONTROL_TYPE_INT,
                poll: 1,
                config: {},
                value: 0
            }
        );
    }
}