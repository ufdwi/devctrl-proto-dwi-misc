import {DWPodiumButtonCommunicator} from "./DW/DWPodiumButtonCommunicator";
import {IA200Communicator} from "./ImageAnyplace/IA200Communicator"
import {AWHE130Communicator} from "./Panasonic/AWHE130Communicator";
import {MXA910Communicator} from "./Shure/MXA910Communicator";
import {EviD31Communicator} from "./Sony/EviD31Communicator";
import {CLQLCommunicator} from "./Yamaha/CLQLCommunicator";
import {Pearl2Communicator} from "./Epiphan/Pearl2Communicator";

//devctrl-proto-package
module.exports = {
    communicators: {
        'DW/PodiumButton' : DWPodiumButtonCommunicator,
        'ImageAnyplace/IA200' : IA200Communicator,
        'Panasonic/AWHE130' : AWHE130Communicator,
        'Shure/MXA910' : MXA910Communicator,
        'Sony/EviD31' : EviD31Communicator,
        'Yamaha/CLQL' : CLQLCommunicator,
        'Epiphan/Pearl2': Pearl2Communicator
    }
};